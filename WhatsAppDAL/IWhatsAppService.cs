﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using WhatsAppDAL.Model;

namespace WhatsAppDAL
{
    public interface IWhatsAppService : IDisposable
    { 
       Task<int> CreateUser (UserViewModel user);

       void UpdateUser (UserViewModel user);

       void DeleteUser (int id);

       UserViewModel GetUser (int id);

       IEnumerable<UserViewModel> GetUsers ();
    }
}